#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

#include "ddtable.h"

#ifndef DT
#define DT 0.01
#endif

// Leak params
#define G_LEAK 1.0
#define E_REV -65.0
#define V_T -50.0
#define DLT_T 0.5
#define ADAPT_COUPL 0.0
#define ADAPT_VAR 0.0
#define TAU_W 0.10
// Compartment Params
#define CM 1.0
#define INIT_VM -65.0
#define MAX_I 0.003
#define MIN_I 0.000

#ifndef CACHE_SIZE
#define CACHE_SIZE 1
#elif CACHE_SIZE < 1024
#warning Cache size too small to be effective
#endif

#if CACHE_SIZE
#define EXP(d, x) ddtable_check_get_set(d, x, &exp)
#else
#define EXP(d, x) exp(x)
#endif

#ifdef RAND_CURR
#define RI (MAX_I - MIN_I + 1) * ((double) rand() / RAND_MAX) + MIN_I
#else
#define RI 0.0
#endif

//! Verify the hash function
static void hash_verify(const int argc, char** argv)
{
    if (argc < 4 || argv == NULL)
    {
#if !CACHE_SIZE
    _hash_verify_fail:
#endif
        fprintf(stderr, "Usage: ddtable_test x_start x_finish x_step.\n");
        exit(EXIT_FAILURE);
    }

#if CACHE_SIZE
    const double x_start = atof(argv[1]);
    const double x_finish = atof(argv[2]);
    const double x_step = atof(argv[3]);

    // Make a table and fill it with values, ignoring collisions
    ddtable_t table = ddtable_new(CACHE_SIZE);
    for (double j = x_start; j < x_finish; j += x_step)
    {
        ddtable_set_val(table, j, exp(j));
    }
    
    int num_mismatches = 0;
    for (double j = x_start; j < x_finish; j += x_step)
    {
        const double _val = ddtable_get_val(table, j);
        if (_val == DDTABLE_NULL_VAL || _val != exp(j))
        {
            num_mismatches++;
        }
    }
    printf("Number of key mismatches: %i, %0.3f%%\n",
           num_mismatches,
           100.0 * num_mismatches / (fabs(x_finish - x_start) / x_step));
    ddtable_free(table);
#else
    goto _hash_verify_fail;
#endif
}

static void hh_simul(const int argc, char** argv)
{
    // Process parameters
    if (argc < 3)
    {
        fprintf(stderr, "Usage: ddtable_test num_cells simul_len\n");
        exit(EXIT_FAILURE);
    }
#if CACHE_SIZE
    ddtable_t table = ddtable_new(CACHE_SIZE);
#endif
    const uint64_t num_cells = atol(argv[1]);
    const uint64_t simul_len = atol(argv[2]);
    
    // Initialize membranes
    double** restrict vm = (double** restrict) malloc(sizeof(double*) * num_cells);
    for (uint64_t i = 0; i < num_cells; i++)
    {
        vm[i] = (double*) calloc(sizeof(double), simul_len);
        vm[i][0] = INIT_VM;
    }

    // Simulate
    for (uint64_t time_step = 1; time_step < simul_len; time_step++)
    {
        for (uint64_t cell_num = 0; cell_num < num_cells; cell_num++)
        {
            const double prev_vm = vm[cell_num][time_step - 1];
            const double g_L = -G_LEAK * (prev_vm - E_REV) + RI;
            const double tmp = (rand() > RAND_MAX / 2)
                ? G_LEAK * DLT_T * EXP(table, (prev_vm - V_T) / DLT_T)
                : 0.0;
            vm[cell_num][time_step] = prev_vm + ((g_L + tmp) * DT) / CM;
        }
    }

    // Dump file
#if CACHE_SIZE    
    FILE* fp = fopen64("ddtable_test.dat", "w");
    fwrite(table->key_vals, sizeof(double), 2 * table->size, fp);
    fclose(fp);
    ddtable_free(table);
#endif

    // Free memory
    for (uint64_t i = 0; i < num_cells; i++)
    {
        free(vm[i]);
    }
    free(vm);
}

int main(int argc, char** argv)
{
    srand(42);

    if (argc == 3)
    {
        hh_simul(argc, argv);
    } else if (argc == 4) {
        hash_verify(argc, argv);
    }

    return EXIT_SUCCESS;
}
