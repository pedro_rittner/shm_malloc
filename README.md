Shared Memory Object Memory API
========================================

`shm_malloc` is a small proof-of-concept memory allocator using a block of POSIX
shared object memory as backing storage for a standard memory allocation
subsystem.

The following functions are currently supported:

* `malloc`

* `calloc`

* `free`

The allocator makes a best-effort to conform to POSIX standards.

Note that memory re-ordering and paging is currently not supported.

How it Works
================

`malloc`
---------

0. We know ahead of time the number of maximum allocations 'live' at once.
   Furthermore, we know the total allocation size limit ahead of time.
   Therefore we pre-allocate a segment at the top to have the following:

    * `sem_t` - synchronization semaphore for thread/process-safe access.

    * `uint32_t offset` - current heap pointer offset.

    * `struct alloc_data* { size_t nbytes; uint32_t offset; }` - allocation metadata.

    * `char[ALLOC_SIZE] heap` - the data heap.

1. Find the heap pointer location and determine if there is enough space left.
   If not, we immediately return NULL and set errno to ERNOMEM.

2. If there is enough space, we must save the current heap pointer location,
   then mark down the address/size in the metadata segment at the top.

3. Finally, we return the pointer to the location of memory we reserved.

`calloc`
---------

This is identical to `malloc`, except we zero the memory with `memset`.
