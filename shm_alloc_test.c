#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <unistd.h>
#include <sched.h>
#include <sys/wait.h>

#define SHM_UTIL_SRC
#include "shm_util.h"

#ifndef ARR_SIZE
#define ARR_SIZE 12
#endif

#ifndef STACK_SIZE
#define STACK_SIZE (1024 * 1024)
#endif

int read_data(void* _ __attribute__((unused)))
{
    struct shmbuf* s = NULL;
    assert((s = shmbuf_map()) != NULL);
    
    puts("I'm the child process.");
    
    // Copy array from buffer to us
    double* arr = calloc(ARR_SIZE, sizeof(double));
    assert(arr != NULL);
    shmbuf_copy_to(s, arr, ARR_SIZE * sizeof(double));

    // Print contents of the array
    for (int i = 0; i < ARR_SIZE; i++)
    {
        printf("child arr[%i] = %f\n", i, arr[i]);
    }

    // Cleanup
    free(arr);
    assert(0 == shmbuf_unmap(s));

    // Exit cleanly
    puts("Child process done.");
    exit(EXIT_SUCCESS);
}

int main(void)
{
    // Only parent process initializes the buffer
    struct shmbuf* s = NULL;    
    assert((s = shmbuf_init()) != NULL);

    // Load array into buffer
    double arr[ARR_SIZE];
    for (int i = 0; i < ARR_SIZE; i++)
    {
        arr[i] = i + 0.0;
        printf("parent arr[%i] = %f\n", i, arr[i]);
    }
    shmbuf_copy_from(s, arr, ARR_SIZE * sizeof(double));

    // Prepare child process stack and call
    char* stack = malloc(STACK_SIZE);
    void* stack_top = stack + STACK_SIZE;     // Stack grows down
    pid_t pid = clone(&read_data,             // Function to fork()
                      stack_top,              // Top of child's stack
                      SIGCHLD | CLONE_FILES,  // Flags
                      NULL);                  // Arguments
    if (pid == -1)
    {
        perror("clone: ");
        exit(EXIT_FAILURE);
    }
    
    puts("I'm the parent process");
    sleep(1);
    
    if (waitpid(pid, NULL, 0) == -1)    // Wait for child
    {
        perror("waitpid");
        exit(EXIT_FAILURE);
    }
    puts("Child has terminated");
    assert(shmbuf_close(s) == 0);
    
    exit(EXIT_SUCCESS);
}
