#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>

#include "mmq.h"

#ifndef ARR_SIZE
#define ARR_SIZE 12
#endif

int write_data(void)
{
    struct mmq_connector conn =
        {
            .msg_queue = mmq_init_mq(),
            .socket_fd = mmq_socket_init(true, NULL),
            .connection_fd = -1,
            .server = true
        };
    
    // Only we initialize the buffer
    puts("Writer process start.");

    // Initialize array
    double* arr = calloc(ARR_SIZE, sizeof(double));
    for (int i = 0; i < ARR_SIZE; i++)
    {
        arr[i] = 0.0 + rand();
        printf("Writer arr[%i] = %f\n", i, arr[i]);
    }


    puts("Waiting for messages on queue...");
    char* msg_buff = malloc(MMQ_MSG_SIZE + 1);
    ssize_t msg_size = mq_receive(conn.msg_queue,
                                  msg_buff,
                                  (size_t) MMQ_MSG_SIZE,
                                  NULL);
    if (msg_size < 0)
    {
        perror("mq_receive:");
        exit(EXIT_FAILURE);
    }
    // Process message
    size_t data_req = 0;
    memcpy(&data_req, msg_buff, sizeof(MMQ_MSG_SIZE));
    memset(msg_buff, 0, sizeof(MMQ_MSG_SIZE + 1));
    printf("Writer data request: %lu\n", data_req);
    
    // Wait for someone to accept our sent data
    mmq_send_data(&conn, arr, data_req);
    puts("Sent data once.");

    
    mmq_send_data(&conn, arr, sizeof(double) * ARR_SIZE);
    puts("Sent data twice.");
    for (int i = 0; i < ARR_SIZE; i++)
    {
        printf("Writer arr[%i] = %f\n", i, arr[i]);
    }
    
    // Cleanup
    free(arr);
    
    // Exit cleanly
    puts("Writer process done.");
    exit(EXIT_SUCCESS);
}

int read_data(void)
{
    puts("Reader process start.");
    double* arr = calloc(ARR_SIZE, sizeof(double));

    // Map buffer
    struct mmq_connector conn =
        {
            .msg_queue = mq_open(MMQ_FNAME, O_RDWR),
            .socket_fd = mmq_socket_init(false, NULL),
            .connection_fd = -1,
            .server = false
        };

    puts("Putting message on queue...");
    size_t data_req = sizeof(double) * ARR_SIZE;
    char* msg_buff = malloc(sizeof(MMQ_MSG_SIZE));
    memcpy(msg_buff, &data_req, sizeof(size_t));
    if (mq_send(conn.msg_queue, msg_buff, sizeof(msg_buff), 0) != 0)
    {
        perror("mq_send failed");
        assert(close(conn.socket_fd) == 0);
        assert(unlink(MMQ_UNSOCK_NAME) == 0);
        assert(mq_unlink(MMQ_FNAME) == 0);
        exit(EXIT_FAILURE);
    }
    
    // Request data
    mmq_request_data(&conn, arr, data_req);
    puts("Read data once.");
    mmq_request_data(&conn, arr, data_req);
    puts("Read data twice.");
    
    // Read data
    for (int i = 0; i < ARR_SIZE; i++)
    {
        printf("Reader arr[%i] = %f\n", i, arr[i]);
    }
    
    // Cleanup
    free(arr);
    
    assert(close(conn.socket_fd) == 0);
    assert(unlink(MMQ_UNSOCK_NAME) == 0);
    assert(mq_unlink(MMQ_FNAME) == 0);
    exit(EXIT_SUCCESS);
}

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "usage: shm_mp_test <0|1>");
        exit(EXIT_FAILURE);
    }
    
    srand(42);
    int writer = 0;
    sscanf(argv[1], "%i", &writer);
    if (writer == 1)
    {
        return write_data();
    } else if (writer == 0){
        return read_data();
    }
}

