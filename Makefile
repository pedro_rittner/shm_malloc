CC := gcc
CFLAGS := -std=gnu99 -Wall -Wextra -Wpedantic -D_POSIX_C_SOURCE=200809L

ifdef DEBUG
CFLAGS += -O0 -g$(DEBUG) -DDEBUG=$(DEBUG)
else
CFLAGS += -O2
endif

DEFINES ?=

LDFLAGS := -L. -L/usr/lib -L/usr/local/lib

LIBS := -lm -lpthread -lrt

INCLUDES := -I/usr/include -I.

BINARIES = ddtable_test myriad_alloc_test mmq_mp_test

.PHONY: all clean reall

all: $(BINARIES)

reall: clean all

%.o: %.c
	$(CC) $(CFLAGS) $(DEFINES) $(INCLUDES) -c $^

ddtable_test: ddtable.o ddtable_test.o
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

myriad_alloc_test: myriad_alloc.o myriad_alloc_test.o
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

# shm_alloc_test: shm_util.o shm_alloc_test.o
# 	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

mmq_mp_test: mmq.o mmq_mp_test.o
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

clean:
	@rm -f $(BINARIES)
	@rm -f *.o
