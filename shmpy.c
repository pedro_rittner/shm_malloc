/**
 * @file pyshm.c
 *
 * @author Pedro Rittner
 *
 * @brief Glue library for interacting with Myriad C code.
 */
#include <python3.4/Python.h>
#include <python3.4/modsupport.h>
#include <numpy/arrayobject.h>

#include <stddef.h>
#include <stdint.h>

#define SHM_UTIL_SRC
#include "shm_util.h"

static PyObject* say_hello(PyObject* self, PyObject* args)
{
    const char* name; 
 
    if (!PyArg_ParseTuple(args, "s", &name))
    {
        return NULL;
    }
 
    printf("Hello %s!\n", name);
 
    Py_RETURN_NONE;
}

static PyObject* read_data(PyObject* self, PyObject* args)
{
    const int data_len;
    if (!PyArg_ParseTuple(args, "i", &data_len))
    {
        return NULL;
    }

    // Initialize buffer and holder array
    struct shmbuf* s = shmbuf_map();
    assert(s != NULL);

    double* arr = PyMem_Malloc(data_len * sizeof(double));

    // Request and receive data
    shmbuf_request_data(s, arr, data_len * sizeof(double), NULL);
    
    // Read data
    npy_intp dims[1] = {data_len};
    PyObject* buf_arr = PyArray_SimpleNewFromData(1,
                                                  dims,
                                                  NPY_FLOAT64,
                                                  arr);
    assert(shmbuf_unmap(s) == 0);

    return buf_arr;
}

static PyMethodDef ShmpyMethods[] =
{
     {"say_hello", say_hello, METH_VARARGS, "Greet somebody."},
     {"read_data", read_data, METH_VARARGS, "Read data from SHM object."},
     {NULL, NULL, 0, NULL}
};

PyDoc_STRVAR(docvar, "Docstring");

struct PyModuleDef shmpy_module_def =
{
    .m_base = PyModuleDef_HEAD_INIT,
    .m_name = "shmpy",
    .m_doc  = docvar,
    .m_size = -1,
    .m_methods = ShmpyMethods,
    .m_reload = NULL,
    .m_traverse = NULL,
    .m_clear = NULL,
    .m_free = NULL
};
 
PyMODINIT_FUNC PyInit_shmpy(void)
{
    _import_array();
    return PyModule_Create(&shmpy_module_def);
}
