#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "myriad_alloc.h"

/*!
 * Allows for unit testing arbitrary functions as long as they follow the
 * standard format of 0 = PASS, anything else = FAIL 
 */
#define UNIT_TEST_FUN(fun, ...) do {                             \
        puts("TESTING: "#fun);                                   \
        const int _ = fun(__VA_ARGS__);                          \
        printf(#fun":\t%s", _ ? "FAIL" : "PASS");                \
        _ ? printf(" @ "__FILE__":%i\n", __LINE__) : puts("");   \
    } while(0)
		
//! Compares the value of two expressions, 0 = PASS, o.w. FAIL
#define UNIT_TEST_VAL_EQ(a, b)  \
    printf("TESTING: "#a" == "#b" ... %s\n", a == b ? "PASS" : "FAIL");

int test_init(const size_t heap_size, const size_t num_alloc)
{   
    UNIT_TEST_VAL_EQ(myriad_alloc_init(heap_size, num_alloc), 0);
    
    assert(myriad_memdat.heap != NULL);
    
    UNIT_TEST_VAL_EQ(myriad_memdat.heap_size, heap_size);
    UNIT_TEST_VAL_EQ(myriad_memdat.offset, 0);

    UNIT_TEST_VAL_EQ(myriad_memdat.metadata_size,
                     num_alloc * sizeof(struct alloc_data));
    
    UNIT_TEST_VAL_EQ(myriad_memdat.meta_indx, 0);

    return 0;
}

int test_alloc(const size_t memsize)
{
    double* d = myriad_malloc(memsize, false);
    assert(d != NULL);

    for (unsigned int i = 0; i < memsize; i++)
    {
        d[i] = 5.0 * i;
    }

    for (unsigned int i = 0; i < memsize; i++)
    {
        assert(d[i] == 5.0 * i);
    }

    myriad_free(d);
    
    return 0;
}

int test_calloc(const size_t num_elems)
{
    unsigned int* arr = myriad_calloc(num_elems, sizeof(unsigned int), false);
    assert(arr != NULL);

    for (unsigned int i = 0; i < num_elems; i++)
    {
        assert(arr[i] == 0);
        arr[i] = 3 * i;
    }

    for (unsigned int i = 0; i < num_elems; i++)
    {
        assert(arr[i] == 3 * i);
    }

    myriad_free(arr);

    return 0;
}

int main(void)
{
    UNIT_TEST_FUN(test_init, 1024, 128);

    UNIT_TEST_FUN(test_alloc, 30);

    UNIT_TEST_FUN(test_calloc, 30);

    UNIT_TEST_FUN(myriad_finalize);

    return EXIT_SUCCESS;
}
